package Day1

fun main()
{
    for(i in -5..5)
    {
        println(calculateY(3, i, -1))
    }
}

fun calculateY(m : Int, x : Int, c : Int) : Int {
    // y = mx + c - formula for a straight line
    return m * x + c
}
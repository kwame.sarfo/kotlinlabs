import kotlin.random.Random

class RandomAgent(override val name:String, val randomNumber:Double):Actor {
    override fun perceive(vararg facts: Percept) {
        TODO("Not yet implemented")
    }

    override fun act(): Action {
        val randNum = Random.nextInt()

        if(randNum/100.0 <= randomNumber)
        {
            return ForageAction()
        }
        else
        {
            return NoAction()
        }
    }

    override fun toString(): String {
        return "$name"
    }
}
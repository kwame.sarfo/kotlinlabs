class FoodEnvironment(vararg args : Actor) : Environment(*args){
    val scores: MutableMap<Actor, Int> = mutableMapOf<Actor, Int>()

    init {
        for(actor in args)
        {
            scores[actor] = 0
        }

    }

    override fun processAction(agent: Actor, act: Action) {
        when(act)
        {
            is ForageAction -> scores[agent] = scores[agent]?.plus(1) ?: 0
            else -> {}
        }

    }

    override fun sense(agent: Actor) {

    }


}
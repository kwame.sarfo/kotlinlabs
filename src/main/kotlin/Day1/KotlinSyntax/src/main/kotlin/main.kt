var funds : Double = 100.0
val pswd = "password"
var userpass : String = ""

fun main() {
    var input : String
    var cmd : List<String>


    while (true) {
        print("Command: ")
        input = readLine()!!
        cmd = input.split(" ")
        when (cmd[0]) {
            "balance" -> balance()
            "deposit" -> deposit(cmd[1].toDouble())
            "withdraw" -> if(cmd.size <=1 || cmd[1].contains(Regex("[a-zA-z]"))) println("Please enter a valid value to withdraw")  else withdraw(cmd[1].toDouble())
            else -> println("Invalid command")
        }
    }
}

fun balance():Unit
{
    println(funds)
}

fun deposit(value:Double)
{
    funds+=value;
}

fun withdraw(value:Double): Unit
{
        print("Please enter a password: ")
        var input = readLine()!!
        if(verifyPassword(input) && value < funds)
        {
            funds-=value
            println("Funds withdrawn successfully")
        }
}

fun verifyPassword(password:String):Boolean
{
    return password == pswd
}